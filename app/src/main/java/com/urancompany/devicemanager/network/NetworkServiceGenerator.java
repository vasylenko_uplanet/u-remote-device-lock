package com.urancompany.devicemanager.network;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;

/**
 * Created by deni on 10/28/16.
 */

public class NetworkServiceGenerator {

    private static Retrofit retrofit = new Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .client(new OkHttpClient.Builder()
                    .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                    .build())
            .baseUrl("http://android.uran.tk/")
            .build();

    public static <T> T getService(Class<T> serviceClass) {
        return retrofit.create(serviceClass);
    }
}
