package com.urancompany.devicemanager.network;

import com.urancompany.devicemanager.policy.DevicePolicyResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by deni on 10/28/16.
 */

public interface ApiService {
    @POST("register")
    Call<BaseResponse> register(@Body Map<String, String> params);

    @POST("getPolicy")
    Call<DevicePolicyResponse> getPolicy(@Body Map<String, String> params);
}
