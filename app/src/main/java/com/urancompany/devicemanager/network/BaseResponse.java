package com.urancompany.devicemanager.network;

/**
 * Created by deni on 10/28/16.
 */

public class BaseResponse {
    private String status;

    private String error;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }

    public boolean isStatusOk() {
        return (status != null && status.equalsIgnoreCase("ok"));
    }
}
