package com.urancompany.devicemanager.messaging;

import android.os.Handler;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.urancompany.devicemanager.R;
import com.urancompany.devicemanager.policy.DeviceManager;
import com.urancompany.devicemanager.policy.DevicePolicyResponse;
import com.urancompany.devicemanager.source.LocalRepository;
import com.urancompany.devicemanager.source.RemoteRepository;

/**
 * Created by deni on 10/28/16.
 */

public class MessagingService extends FirebaseMessagingService {
    Handler mHandler;

    @Override
    public void onCreate() {
        super.onCreate();
        mHandler = new Handler();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (isPolicyMessage(remoteMessage)) {
            checkNewPolicy();
        } else {
            super.onMessageReceived(remoteMessage);
        }
    }

    private boolean isPolicyMessage(RemoteMessage remoteMessage) {
        String policyStatus = null;
        if (remoteMessage != null && remoteMessage.getData() != null) {
            policyStatus = remoteMessage.getData().get("policy");
        }
        return (policyStatus != null && policyStatus.equalsIgnoreCase("updated"));
    }

    private void checkNewPolicy() {
        DevicePolicyResponse response = new RemoteRepository().getPolicy(new LocalRepository().getToken(this));
        if (response != null && response.isStatusOk()) {
            if (response.getData() != null && response.getData().isInstantLock()) {
                if (DeviceManager.isAdminActive(this)) {
                    DeviceManager.lockDevice(this);
                } else {
                    showAlert(getString(R.string.alert_lock_no_permission));
                }
            }
        }
    }

    private void showAlert(final String message) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MessagingService.this, message, Toast.LENGTH_LONG).show();
            }
        });
    }
}
