package com.urancompany.devicemanager.messaging;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.urancompany.devicemanager.source.LocalRepository;
import com.urancompany.devicemanager.source.RemoteRepository;

import static com.urancompany.devicemanager.utils.DeviceInfoUtils.getDeviceName;

/**
 * Created by deni on 10/28/16.
 */

public class FirebaseIdService extends FirebaseInstanceIdService {
    private static final String TAG = "FirebaseIdService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        saveRegistrationTokenLocal(refreshedToken);
    }
    // [END refresh_token]

    private void saveRegistrationTokenLocal(String token) {
        new LocalRepository().saveToken(this, token);
    }
}
