package com.urancompany.devicemanager.home;

import android.content.Context;

/**
 * Created by deni on 10/30/16.
 */

public class HomeContract {
    interface View {
        Context getViewContext();

        void showAlert(String message);

        void setPermissionButtonTitle(String title);

        void showPermissionDialog(int requestCode);

        void showRevokePermissionDialog();
    }

    interface Presenter {
        void onGrantPermissionClick();

        void onServerRegisterClick();

        void onResume();
    }
}
