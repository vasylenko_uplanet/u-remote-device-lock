package com.urancompany.devicemanager.home;

import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.urancompany.devicemanager.network.BaseResponse;
import com.urancompany.devicemanager.R;
import com.urancompany.devicemanager.policy.DeviceManager;
import com.urancompany.devicemanager.utils.DeviceInfoUtils;
import com.urancompany.devicemanager.utils.PreferencesManager;

/**
 * Created by deni on 10/30/16.
 */

public class HomePresenter implements HomeContract.Presenter {
    private static final int PERMISSION_CODE_REQUEST = 1;
    private HomeContract.View mView;
    private HomeDataSource mHomeDataSource;

    private SharedPreferences.OnSharedPreferenceChangeListener listener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
            if (key != null && key.equalsIgnoreCase(PreferencesManager.PERMISSION)) {
                onPermissionStatusChanged();
            }
        }
    };

    HomePresenter(HomeContract.View view, HomeDataSource dataSource) {
        mView = view;
        mHomeDataSource = dataSource;
        PreferencesManager.getInstance(view.getViewContext()).getPreferences().registerOnSharedPreferenceChangeListener(listener);
    }

    private void grantPermission() {
        if (!DeviceManager.isAdminActive(mView.getViewContext())) {
            mView.showPermissionDialog(PERMISSION_CODE_REQUEST);
        } else {
            mView.showRevokePermissionDialog();
        }
    }

    void onPermissionStatusChanged() {
        if (PreferencesManager.getInstance(mView.getViewContext()).getBoolean(PreferencesManager.PERMISSION)) {
            mView.setPermissionButtonTitle(mView.getViewContext().getString(R.string.revoke_admin_permission));
        } else {
            mView.setPermissionButtonTitle(mView.getViewContext().getString(R.string.grant_admin_permission));
        }
    }

    @Override
    public void onGrantPermissionClick() {
        grantPermission();
    }

    @Override
    public void onServerRegisterClick() {
        new AsyncTask<Void, Void, BaseResponse>() {

            @Override
            protected BaseResponse doInBackground(Void... params) {
                return mHomeDataSource.register(mHomeDataSource.getToken(), DeviceInfoUtils.getDeviceName());
            }

            @Override
            protected void onPostExecute(BaseResponse baseResponse) {
                super.onPostExecute(baseResponse);
                if (baseResponse.isStatusOk()) {
                    if (mView != null) {
                        mView.showAlert(mView.getViewContext().getString(R.string.alert_device_registered));
                    }
                } else {
                    if (mView != null) {
                        mView.showAlert("Error: " + baseResponse.getError());
                    }
                }
            }
        }.execute();

    }

    @Override
    public void onResume() {
        PreferencesManager.getInstance(mView.getViewContext()).saveBoolean(PreferencesManager.PERMISSION, DeviceManager.isAdminActive(mView.getViewContext()));
        onPermissionStatusChanged();
    }
}
