package com.urancompany.devicemanager.home;

import android.content.Context;

import com.urancompany.devicemanager.network.BaseResponse;
import com.urancompany.devicemanager.source.LocalDataSource;
import com.urancompany.devicemanager.source.RemoteDataSource;

/**
 * Created by deni on 10/30/16.
 */

public class HomeRepository implements HomeDataSource {
    RemoteDataSource mRemoteDataSource;
    LocalDataSource mLocalDataSource;
    Context mContext;

    public HomeRepository(Context context, RemoteDataSource remoteDataSource, LocalDataSource localDataSource) {
        mRemoteDataSource = remoteDataSource;
        mLocalDataSource = localDataSource;
        mContext = context;
    }

    @Override
    public String getToken() {
        return mLocalDataSource.getToken(mContext);
    }

    @Override
    public BaseResponse register(String token, String deviceName) {
        return mRemoteDataSource.register(deviceName, token);
    }
}
