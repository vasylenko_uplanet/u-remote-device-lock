package com.urancompany.devicemanager.home;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.urancompany.devicemanager.R;
import com.urancompany.devicemanager.policy.DeviceAdminManagerReceiver;
import com.urancompany.devicemanager.policy.DeviceManager;
import com.urancompany.devicemanager.source.LocalRepository;
import com.urancompany.devicemanager.source.RemoteRepository;
import com.urancompany.devicemanager.utils.PreferencesManager;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, HomeContract.View {
    HomeContract.Presenter mPresenter;

    Button mPermissionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.register).setOnClickListener(this);
        mPermissionButton = (Button) findViewById(R.id.permission);
        mPermissionButton.setOnClickListener(this);

        mPresenter = new HomePresenter(this, new HomeRepository(getApplicationContext(), new RemoteRepository(), new LocalRepository()));//Should be handled by DI framework
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register:
                mPresenter.onServerRegisterClick();
                break;
            case R.id.permission:
                mPresenter.onGrantPermissionClick();
                break;
        }
    }

    @Override
    public Context getViewContext() {
        return this;
    }

    @Override
    public void showAlert(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setPermissionButtonTitle(String title) {
        mPermissionButton.setText(title);
    }

    @Override
    public void showPermissionDialog(int requestCode) {
        // Launch the activity to have the user enable our admin.
        Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, DeviceManager.getDeviceAdminComponent(this));
        intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
                getString(R.string.grant_admin_permission));
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void showRevokePermissionDialog() {
        DevicePolicyManager dpm = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        dpm.removeActiveAdmin(DeviceManager.getDeviceAdminComponent(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.onResume();
    }
}
