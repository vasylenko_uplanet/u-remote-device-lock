package com.urancompany.devicemanager.home;

import com.urancompany.devicemanager.network.BaseResponse;

/**
 * Created by deni on 10/30/16.
 */

public interface HomeDataSource {
    String getToken();

    BaseResponse register(String token, String deviceName);
}
