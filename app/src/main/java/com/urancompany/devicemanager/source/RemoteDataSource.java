package com.urancompany.devicemanager.source;

import com.urancompany.devicemanager.policy.DevicePolicyResponse;
import com.urancompany.devicemanager.network.BaseResponse;

/**
 * Created by deni on 10/29/16.
 */

public interface RemoteDataSource {
    BaseResponse register(String deviceName, String token);

    DevicePolicyResponse getPolicy(String token);
}
