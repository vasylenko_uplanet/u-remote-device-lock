package com.urancompany.devicemanager.source;

import android.content.Context;

/**
 * Created by deni on 10/29/16.
 */

public interface LocalDataSource {
    void saveToken(Context context, String token);

    String getToken(Context context);
}
