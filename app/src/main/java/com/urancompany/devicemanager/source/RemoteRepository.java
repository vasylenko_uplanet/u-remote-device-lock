package com.urancompany.devicemanager.source;

import com.urancompany.devicemanager.policy.DevicePolicyResponse;
import com.urancompany.devicemanager.network.ApiService;
import com.urancompany.devicemanager.network.BaseResponse;
import com.urancompany.devicemanager.network.NetworkServiceGenerator;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by deni on 10/29/16.
 */

public class RemoteRepository implements RemoteDataSource {

    @Override
    public BaseResponse register(String deviceName, String token) {
        Map<String, String> params = new HashMap<>();
        params.put("device_id", token);
        params.put("description", deviceName);
        Call<BaseResponse> call = NetworkServiceGenerator.getService(ApiService.class).register(params);
        try {
            Response<BaseResponse> response = call.execute();
            if (response.isSuccess()) {
                return response.body();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public DevicePolicyResponse getPolicy(String token) {
        Map<String, String> params = new HashMap<>();
        params.put("device_id", token);
        Call<DevicePolicyResponse> call = NetworkServiceGenerator.getService(ApiService.class).getPolicy(params);
        try {
            Response<DevicePolicyResponse> response = call.execute();
            if (response.isSuccess()) {
                return response.body();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
