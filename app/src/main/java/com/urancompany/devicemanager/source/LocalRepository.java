package com.urancompany.devicemanager.source;

import android.content.Context;

import com.urancompany.devicemanager.utils.PreferencesManager;

/**
 * Created by deni on 10/29/16.
 */

public class LocalRepository implements LocalDataSource {
    @Override
    public void saveToken(Context context, String token) {
        PreferencesManager.getInstance(context).saveString(PreferencesManager.TOKEN, token);
    }

    @Override
    public String getToken(Context context) {
        return PreferencesManager.getInstance(context).getString(PreferencesManager.TOKEN);
    }
}
