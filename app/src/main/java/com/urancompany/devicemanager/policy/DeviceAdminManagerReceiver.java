package com.urancompany.devicemanager.policy;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.urancompany.devicemanager.R;
import com.urancompany.devicemanager.utils.PreferencesManager;

/**
 * Created by deni on 10/28/16.
 */

public class DeviceAdminManagerReceiver extends DeviceAdminReceiver {
    void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEnabled(Context context, Intent intent) {
        showToast(context, context.getString(R.string.alert_permission_granted));
        PreferencesManager.getInstance(context).saveBoolean(PreferencesManager.PERMISSION, true);
    }

    @Override
    public void onDisabled(Context context, Intent intent) {
        showToast(context, context.getString(R.string.alert_permission_invoked));
        PreferencesManager.getInstance(context).saveBoolean(PreferencesManager.PERMISSION, false);
    }

}
