package com.urancompany.devicemanager.policy;

import com.urancompany.devicemanager.network.BaseResponse;

/**
 * Created by deni on 10/29/16.
 */

public class DevicePolicyResponse extends BaseResponse {
    DevicePolicy data;

    public static class DevicePolicy {
        private boolean instantLock;

        public boolean isInstantLock() {
            return instantLock;
        }

        public void setInstantLock(boolean instantLock) {
            this.instantLock = instantLock;
        }
    }

    public DevicePolicy getData() {
        return data;
    }
}
