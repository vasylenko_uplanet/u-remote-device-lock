package com.urancompany.devicemanager.policy;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;

/**
 * Created by deni on 10/29/16.
 */

public final class DeviceManager {
    private static ComponentName mDeviceAdminComponent;
    private static DevicePolicyManager mDPM;

    public static ComponentName getDeviceAdminComponent(Context context) {
        if (mDeviceAdminComponent == null) {
            mDeviceAdminComponent = new ComponentName(context.getApplicationContext(), DeviceAdminManagerReceiver.class);
        }
        return mDeviceAdminComponent;
    }

    static DevicePolicyManager getDpm(Context context) {
        if (mDPM == null) {
            mDPM = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        }
        return mDPM;
    }

    public static boolean isAdminActive(Context context) {
        return getDpm(context).isAdminActive(getDeviceAdminComponent(context));
    }

    public static void lockDevice(Context context) {
        if (isAdminActive(context)) {
            getDpm(context).lockNow();
        }
    }

}
