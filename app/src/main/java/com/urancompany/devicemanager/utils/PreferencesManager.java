package com.urancompany.devicemanager.utils;

import android.content.Context;
import android.content.SharedPreferences;


public final class PreferencesManager {
    private static final String PREF_NAME = "com.urancompany.devicemanager.PREF";

    public static final String TOKEN = "token";
    public static final String PERMISSION = "permission";

    private static PreferencesManager sInstance;
    private final SharedPreferences prefs;

    private PreferencesManager(Context context) {
        prefs = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public SharedPreferences getPreferences(){
        return prefs;
    }

    public static synchronized PreferencesManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new PreferencesManager(context);
        }
        return sInstance;
    }

    public void clear() {
        SharedPreferences.Editor e = prefs.edit();
        e.clear();
        e.commit();
    }

    public void saveBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public void saveString(String key, String value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void saveFloat(String key, float value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putFloat(key, value);
        editor.commit();
    }

    public String getString(String key) {
        return prefs.getString(key, null);
    }

    public boolean getBoolean(String key) {
        return prefs.getBoolean(key, false);
    }

    public float getFloat(String key) {
        return prefs.getFloat(key, 0);
    }

    public void saveLong(String key, long value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong(key, value);
        editor.commit();
    }

    public void saveInt(String key, int value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public long getLong(String key) {
        return prefs.getLong(key, 0);
    }

    public int getInt(String key) {
        return prefs.getInt(key, 0);
    }

}
