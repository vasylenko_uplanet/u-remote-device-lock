package com.urancompany.devicemanager.utils;

import android.os.Build;

/**
 * Created by deni on 10/29/16.
 */

public class DeviceInfoUtils {
    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        String name;
        if (model.startsWith(manufacturer)) {
            name = model.toUpperCase();
        } else {
            name = manufacturer.toUpperCase() + " " + model.toUpperCase();
        }
        return name;
    }
}
